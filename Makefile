DEV_SETTINGS=wwm.settings.dev
PROD_SETTINGS=wwm.settings.prod

debian:
	sudo apt-get install mkvirtualenv

install:
	pip install -r wwm/requirements.txt

install-dev: install
	pip install -r wwm/requirements-dev.txt

pull:
	git pull --rebase

migrate:
	python manage.py makemigrations --settings=$(DEV_SETTINGS)
	python manage.py migrate --settings=$(DEV_SETTINGS)

# update the repo and the DB
update: pull install migrate

update-dev: pull install-dev migrate

run:
	python manage.py runserver --settings=$(DEV_SETTINGS)

run-prod:
	python manage.py runserver --settings=$(PROD_SETTINGS)
