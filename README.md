On souhaite une interface d'administration pour le formulaire
`wifi-with-me` de Tetaneutral. (voir [l'appli bottle](https://github.com/JocelynDelalande/django-project-template))

Or, le framework Bottle, utilisé pour le formulaire d'inscription,
n'en fournit pas. On va premièrement partager une BD entre Django
et Bottle pour garder le formulaire et bénéficier de l'appli
d'administration qui vient par défaut avec Django, puis tout refaire en Django.


Cloner et installer le projet
===

    git clone git@gitlab.com:vindarel/wifi-with-django.git

On peut créer un environnement virtuel python afin de ne pas lancer pip en
root, mais ce n'est pas obligé::

    make debian  # install mkvirtualenv
    mkvirtualenv wifi
    workon wifi

Le prompt du shell devrait avoir changé et indiquer "wifi".

Installer les dépendances:

    make install  # installe les librairies python avec pip

Base de données
===

Après un git clone, on n'a pas de BD, pas de super utilisateur pour se
logguer. Soit on utilise une BD présente (par exemple celle qu'on
partage avec l'app Bottle), soit on l'initialise:

    ./manage.py migrate

Créer un super user pour l'admin:

    ./manage.py createsuperuser

Partager la BD entre Django et Bottle
---

On peut les partager avec un lien physique (`ln` tout court, pas `ln -s`):

    ln <nom bd existantes> <nom de la cible>

- créer la BD côté Django
- dans l'appli Bottle, modifier le nom de la table `TABLE_NAME` (ici,
  `wifi_home`, qui correspond au nom de l'app Django (du dossier) + le
  nom de la classe qui définit nos modèles)


Mettre à jour la BD
---

Quand on modifie le modèle Django, le fichier sqlite n'est pas
directement modifié, il faut lui demander de migrer:

    git pull --rebase
    ./manage.py makemigrations # crée un fichier migrations
    ./manage.py migrate

Note:: ces commandes sont rassemblées dans `make update`

Serveur de test
===

On peut lancer le serveur de test:

    make run


et ouvrir notre navigateur sur le port indiqué.

Utiliser l'interface d'admin
===

Notre but est de se connecter à l'application d'admin, sur l'url
`/admin/`. De là on a la liste des réponses au questionnaire.

Mettre en prod
===

Nous ne devons pas utiliser les mêmes settings entre la prod et le
développement en local. Pour la prod, utiliser

    make run-prod

Autres outils
===

- `sqlitebrowser`, `gitk`,…
