# -*- coding: utf-8 -*-
from django import forms
from django.core.urlresolvers import reverse
from django.forms import ModelForm
from django.shortcuts import redirect
from django.shortcuts import render
from wifi.models import Connexion

ORIENTATIONS = [
    ('360', 'Vue à 360°'),
    ('Nord', 'Nord'),
    ('Nord-ouest', 'Nord-Ouest'),
    ('Ouest', 'Ouest'),
    ('Sud-Ouest', 'Sud-Ouest'),
    ('Sud', 'Sud'),
    ('Sud-Est', 'Sud-Est'),
    ('Eest', 'Est'),
    ('Nord-Est', 'Nord-Est'),
]

# Angular sector for each direction, written as (start, stop) in degrees
ANGLES = {
     'N':  (-23, 22),
     'NO': (292, 337),
     'O':  (247, 292),
     'SO': (202, 247),
     'S':  (157, 202),
     'SE': (112, 157),
     'E':  (67, 112),
     'NE': (22, 67)
}


class HomeForm(ModelForm):
    """formulaire construit directement depuis la déclaration du modèle
    https://docs.djangoproject.com/en/1.9/topics/forms/modelforms/
    """
    class Meta:
        model = Connexion
        # fields = ['name']
        exclude = ['status']

    orientations = forms.MultipleChoiceField(choices=ORIENTATIONS, required=True,
                                             widget=forms.CheckboxSelectMultiple(),
                                             label="Orientations de mes fenêtres, balcons et véluxes")

def home(request):
    """
    """
    template = "wifi/home.html"
    # template = "wifi/base.html"
    req = request.POST.copy()
    form = HomeForm()
    if request.method == "POST":
        form = HomeForm(req)
        # a finir TODO:
        if form.is_valid():
            if not form.cleaned_data.get("connect_internet"):
                form.cleaned_data["connect_internet"] = 0
            try:
                maison = HomeForm(**form.cleaned_data)
                maison.save()
            except Exception as e:
                print "Error on registering a new Home: {}".format(e) # this should be a log TODO:
                return render(request, form) # display an error message

            return redirect(reverse("completed"))

    return render(request, template, {
        "form": form,
        })

def success(request):
    tpl = "wifi/success.html"
    return render(request, tpl)
