# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

MAXCHAR = 99

class Connexion(models.Model):
    """doc :)
    """

    name = models.CharField(max_length=MAXCHAR)
    contrib_type = models.CharField(max_length=MAXCHAR, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    phone = models.CharField(max_length=MAXCHAR, blank=True, null=True)
    email = models.CharField(max_length=MAXCHAR, blank=True, null=True)
    access_type = models.CharField(max_length=MAXCHAR, blank=True, null=True)
    floor = models.IntegerField(blank=True, null=True)
    floor_total = models.IntegerField(blank=True, null=True)
    # ce pourrait être un champ many to many,
    # à la place on a la liste des choix en texte.
    orientations = models.CharField(max_length=MAXCHAR, blank=True, null=True)
    roof = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    privacy_name = models.IntegerField(blank=True, null=True) # bool ? use BooleanField
    privacy_email = models.IntegerField(blank=True, null=True)
    privacy_coordinates = models.IntegerField(blank=True, null=True)
    privacy_place_details = models.IntegerField(blank=True, null=True)
    privacy_comment = models.IntegerField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)

    CONNECTABILITY = (
		    ('A_ETUDIER','à étudier'),
		    ('A_CONNECTER','à connecter'),
		    ('CONNECTE', 'connecté'),
		    ('PAS_CONNECTABLE', 'pas connectable'),
		    )

    status = models.CharField(blank=True, null=True, max_length= 250, choices=CONNECTABILITY, default='A_ETUDIER')

    class Meta:
        app_label = "wifi"
