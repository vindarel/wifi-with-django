from django.contrib import admin

# Register your models here.
from .models import Connexion

class HomeAdmin(admin.ModelAdmin):
    class Meta:
        model = Connexion

    search_fields = ["name", "email", "phone"]
    list_display = ("name", "date",)
    # list_editable = ("", "",)
    # filter_horizontal = ("name",)

admin.site.register(Connexion, HomeAdmin)
